#!/bin/bash

filename="/tmp/log_$(date +%s).json"

###################     helpFunction     ###################

function helpFunction() {
    echo ""
    echo "Usage: $0 job-static-analysis/job-sca-scan/job-secrets-scan/job-container-scan"
    exit 1
}

###################     job-static-analysis     ###################

function job-static-analysis() {
    tee $filename <<EOF
{
"job_id":"$CI_JOB_ID",
"job_name":"job-static-analysis",
"pipeline_id":"$CI_PIPELINE_ID",
"project_id":"$CI_PROJECT_ID",
"commit_branch":"$CI_COMMIT_REF_NAME"
}
EOF
}

###################     job-sca-scan     ###################

function job-sca-scan() {
    tee $filename <<EOF
{
"job_id":"$CI_JOB_ID",
"job_name":"job-sca-scan",
"pipeline_id":"$CI_PIPELINE_ID",
"project_id":"$CI_PROJECT_ID",
"commit_branch":"$CI_COMMIT_REF_NAME",
"sca_policy_violation":0
}
EOF
}

###################     job-secrets-scan     ###################

function job-secrets-scan() {
    echo "---------VULNERABILITY_COUNT---------"
    [ -e "/tmp/VULNERABILITY_COUNT.txt" ] && VULNERABILITY_COUNT=$(cat /tmp/VULNERABILITY_COUNT.txt) || (echo "Error: file not found" && exit 0)
    [ "$VULNERABILITY_COUNT" == "null" ] && VULNERABILITY_COUNT=0
    VULNERABILITY_COUNT=${VULNERABILITY_COUNT:-0}

    tee $filename <<EOF
{
"job_id":"$CI_JOB_ID",
"job_name":"job-secrets-scan",
"pipeline_id":"$CI_PIPELINE_ID",
"project_id":"$CI_PROJECT_ID",
"commit_branch":"$CI_COMMIT_REF_NAME",
"hardcoded_secrets_count":"$VULNERABILITY_COUNT"
}
EOF
}

###################     job-container-scan     ###################

function job-container-scan() {
    echo "---------VULNERABILITY_SUMMARY---------"

    VULNERABILITY_SUMMARY=$(cat aqua-report.json | jq -r '.vulnerability_summary')

    critical=$(echo $VULNERABILITY_SUMMARY | jq -r '.critical')
    high=$(echo $VULNERABILITY_SUMMARY | jq -r '.high')
    medium=$(echo $VULNERABILITY_SUMMARY | jq -r '.medium')
    low=$(echo $VULNERABILITY_SUMMARY | jq -r '.low')
    negligible=$(echo $VULNERABILITY_SUMMARY | jq -r '.negligible')

    [ "$critical" == "null" ] && critical=0
    [ "$high" == "null" ] && high=0
    [ "$medium" == "null" ] && medium=0
    [ "$low" == "null" ] && low=0
    [ "$negligible" == "null" ] && negligible=0

    tee $filename <<EOF
{
"job_id":"$CI_JOB_ID",
"job_name":"job-container-scan",
"pipeline_id":"$CI_PIPELINE_ID",
"project_id":"$CI_PROJECT_ID",
"commit_branch":"$CI_COMMIT_REF_NAME",
"container_vuln_critical":"$critical",
"container_vuln_high":"$high",
"container_vuln_medium":"$medium",
"container_vuln_low":"$low",
"container_vuln_negligible":"$negligible"
}
EOF
}

################
# START SCRIPT #
################

if [[ $# -lt 1 ]]; then
    helpFunction
fi

while [[ $# -gt 0 ]]
do
key="$1"
    case $key in
        job-static-analysis)
        job-static-analysis
        shift
        ;;

        job-sca-scan)
        job-sca-scan
        shift
        ;;

        job-secrets-scan)
        job-secrets-scan
        shift
        ;;

        job-container-scan)
        job-container-scan
        shift
        ;;

        * )
        helpFunction
        ;;
    esac
done

#curl --request POST --header "Authorization: Basic 598ccd83c2d18b5d10cf995c3f8d53df077724fb" --header "Content-Type: application/json" "https://compliance.amansingh41/api/compliance/push" --data "@$filename"
