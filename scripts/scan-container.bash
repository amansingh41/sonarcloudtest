#!/bin/bash

export TRIVY_VERSION=$(wget -qO - "https://api.github.com/repos/aquasecurity/trivy/releases/latest" | grep '"tag_name":' | sed -E 's/.*"v([^"]+)".*/\1/')
wget --no-verbose https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz -O - | tar -zxvf -
./trivy image --no-progress $1 &> trivy_log

docker run --rm -v ${CI_PROJECT_DIR}:/tmp -v /var/run/docker.sock:/var/run/docker.sock $AQUASCANNER_IMAGE scan \
  --local $1 --host $AQUA_HOST --token $AQUA_TOKEN --collect-executables --show-negligible --layer-vulnerabilities \
  --htmlfile /tmp/aqua-report.html --jsonfile /tmp/aqua-report.json >  /dev/null
VULNERABILITY_SUMMARY=$(cat aqua-report.json | jq -r '.vulnerability_summary')
VULNERABILITY_COUNT=$(echo $VULNERABILITY_SUMMARY | jq length)
RANGE=$(($VULNERABILITY_COUNT-1))

BINARY_POSITION=$(awk '/=========/{c++} c==2{print NR;exit}' trivy_log)
LINE_NUMBER=$(($BINARY_POSITION-2))

if [[ "$LINE_NUMBER" -eq -2 ]]; then
    echo "-------No binary vulnerabilities were identified by Trivy in this image-------" > trivy_binary_scan.html
else
    tail -n +"$LINE_NUMBER" trivy_log > trivy_binary_scan.html
fi

if grep -q 'Total: [1-9]' trivy_binary_scan.html; then
    echo "-------Binary vulnerabilities were identified by Trivy in this image. Please see trivy_binary_scan.html for more information.-------"
else
    echo "-------No binary vulnerabilities were identified by Trivy in this image-------"
fi

if [ $VULNERABILITY_COUNT -lt 1 ]; then
    echo "-------No vulnerabilities were identified by Aqua in this image-------"
else
    echo "-------Vulnerabilities were identified by Aqua in this image. Please see aqua-report.html for more information.-------"
    echo $VULNERABILITY_SUMMARY | tr -d '{}"'
fi

