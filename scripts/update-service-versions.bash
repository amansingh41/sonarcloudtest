#!/bin/bash
echo "Image = $CI_REGISTRY_IMAGE"
curl --header "PRIVATE-TOKEN: $VERSION_UPDATE_TOKEN" "https://gitlab.com/api/v4/projects/$2/repository/files/pds-service-versions/raw?ref=develop" -o pds-service-versions
FULL_IMAGE_TAG="$CI_REGISTRY_IMAGE:$1"
echo "Tag = $FULL_IMAGE_TAG"
grep "^$CI_REGISTRY_IMAGE" pds-service-versions && sed -i "s|$CI_REGISTRY_IMAGE:.*|$FULL_IMAGE_TAG|g" pds-service-versions || echo "$FULL_IMAGE_TAG" >> pds-service-versions

curl --request PUT --header "PRIVATE-TOKEN:$VERSION_UPDATE_TOKEN" -F "branch=develop" -F "author_email=amans@technotrios.com" -F "author_name=Secure CI" -F "content=<pds-service-versions" -F "commit_message=$CI_PROJECT_NAME version updated" "https://gitlab.com/api/v4/projects/$2/repository/files/pds-service-versions"
